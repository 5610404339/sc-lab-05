package labbp5;

public class Instructor {
	String name ;
	String lastname ;
	String dep_id ;
	public Instructor(String name, String lastname, String dep_id) {
		this.name = name ;
		this.lastname = lastname ;
		this.dep_id = dep_id ;
	}
	public String get_instructorname(){
		return name ;
	}
	public String get_instructorlastname(){
		return lastname ;
	}
	public String get_instructordep_id(){
		return dep_id ;
	}
	
	

}
